variable "vpc_id" {
  type = "string"
}

variable "tags_igw" {
  type    = "string"
  default = "ProjetoDW"
}
