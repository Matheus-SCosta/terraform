variable "inst_ami" {
  description = "Instance ami"
  type        = string
}

variable "inst_type" {
  description = "Instance family type"
  type        = string
}

variable "inst_subnet" {
  description = "Instance subnet"
  type        = string
}

variable "security_groups" {
  description = "Security groups"
  type        = list(any)
}

variable "inst_name" {
  description = "Instance name"
  type        = string
}

variable "instance_role" {
  description = "Instance IAM profile"
  type        = string
}

# True por padrão
variable "delete_on_termination" {
  description = "setting to customize the root disk"
  type        = bool
}

# False por padrão
variable "encrypted" {
  description = "setting to customize the root disk"
  type        = bool
}

# Válido apenas se o volume_tipe for io1, io2 ou gp3, sendo gp2 seu valor deverá ser null. Para io1 o indicado é que seja igual ou maior que 400, para io2 indicado ser igual ou maior que 4000, stardart não possui essa configuração.
variable "iops" {
  description = "setting to customize the root disk"
  type        = number
}


# Válido apenas se o valor de encrypted for true. Caso seja false, o valor atribuido a kms_key_id deverá ser true
variable "kms_key_id" {
  description = "setting to customize the root disk"
  type        = string
}

# Valido apenas se o volume_tipe for gp3. Para gp2, io1 ou io2 que não possuem essa configuração seu valor deverá ser null;
variable "throughput" {
  description = "setting to customize the root disk"
  type        = number
}


variable "volume_size" {
  description = "setting to customize the root disk"
  type        = number
}


variable "volume_type" {
  description = "setting to customize the root disk"
  type        = string
}

variable "tags_root_disk" {
  description = "setting to customize the root disk"
  type        = map(string)
}

